/*
 * Created on : Dec 28, 2017, 9:49:09 AM
 * Author: Tran Trong Thang
 * Email: trantrongthang1207@gmail.com
 * Skype: trantrongthang1207
 */
jQuery(document).ready(function ($) {
    /*============================================================================
     Home page - slideshow
     ==============================================================================*/

    var slideshow = {
        init: function () {
            $('.homepage_slider').each(function (index, value) {
                var homepageSlider = $(this);
                var slideshowAnimation = homepageSlider.data('slideshow-animation');
                var slideshowSpeed = homepageSlider.data('slideshow-speed');

                homepageSlider.flexslider({
                    touch: true,
                    controlNav: true,
                    animation: slideshowAnimation,
                    slideshowSpeed: slideshowSpeed ? slideshowSpeed * 1000 : 10 * 1000,
                    prevText: '<span class="icon-circle-left"></span>',
                    nextText: '<span class="icon-circle-right"></span>',
                    start: function () {
                        $('.homepage_slider').removeClass('slider-loading')
                    }
                });

                if ($('.slides li', homepageSlider).length == 1) {
                    $('.flex-direction-nav', homepageSlider).hide()
                }
            });

        },
        unload: function ($target) {
            var slider = $target.find('.homepage_slider');
            destroySlider(slider);
        }
    }
    var animate_content = {
        init: function () {
            if ($(window).width() >= 768 || $(window).width() == 0) {
                $(".animate_right").waypoint(function () {
                    $(this.element).addClass("animated fadeInRight");
                }, {offset: '80%'});
                $(".animate_left").waypoint(function () {
                    $(this.element).addClass("animated fadeInLeft");
                }, {offset: '80%'});
                $(".animate_up").waypoint(function () {
                    $(this.element).addClass("animated fadeInUp");
                }, {offset: '80%'});
                $(".animate_in").waypoint(function () {
                    $(this.element).addClass("animated fadeIn");
                }, {offset: '80%'});
                $(".animate_down").waypoint(function () {
                    $(this.element).addClass("animated fadeInDown");
                }, {offset: '80%'});
            }
        }
    }
    var banner = {
        init: function () {
            if ($('.parallax-banner').hasClass('parallax_effect--true')) {
                if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && $(window).width() >= 768) {
                    skrollr.init({forceHeight: false});

                    var s = skrollr.init({forceHeight: false});
                    s.refresh($(".bcg"));
                    $(".slideshow-section").waypoint(function () {
                        var evt = document.createEvent('UIEvents');
                        evt.initUIEvent('resize', true, false, window, 0);
                        window.dispatchEvent(evt);
                    }, {offset: '100%'});
                } else if ($(window).width() >= 768) {
                    $('.parallax-banner').removeClass('parallax_effect--true').addClass('parallax_effect--false');
                }
            }

            //promo banner is enabled
            if ($('.promo_banner').length) {
                $('body').addClass('promo_banner--active');
            } else {
                $('body').removeClass('promo_banner--active');
            }

            animate_content.init();
        },
        unload: function ($target) {
        }
    }
    slideshow.init();
    banner.init();

    var instagram = {
        loadContent: function (s) {
            if (s.clientID) {
                var url = 'https://api.instagram.com/v1/users/self/media/recent/?access_token=' + s.clientID;

                $.ajax({
                    type: 'GET',
                    url: url,
                    dataType: 'jsonp',
                    success: function (data) {

                        if (data.meta.code === 200 && data.data.length) {
                            var data = data.data;
                            var count = 0;
                            s.el.empty();

                            for (var i = 0; i < data.length; i++) {
                                var thisMedia = data[i], item;

                                if (!thisMedia.images.thumbnail.url.indexOf("null") > -1) {
                                    var url = thisMedia.images.thumbnail.url;
                                    url = url.replace(/\/s150x150\//, "/s320x320/");

                                    item = '<img class="il-photo__img" src="' + url + '" data-filter="' + thisMedia.filter + '" />';
                                    item = '<a href="' + thisMedia.link + '" target="_blank">' + item + '</a>';
                                }


                                if (thisMedia.videos) {
                                    var src;

                                    if (thisMedia.videos.standard_resolution) {
                                        src = thisMedia.videos.standard_resolution.url;
                                    } else if (thisMedia.videos.low_resolution) {
                                        src = thisMedia.videos.low_resolution.url;
                                    } else if (thisMedia.videos.low_bandwidth) {
                                        src = thisMedia.videos.low_bandwidth.url;
                                    }

                                    var videothumb = thisMedia.images.thumbnail.url;
                                    videothumb = videothumb.replace(/\/s150x150\//, "/s320x320/");

                                    item = '<div class="vidholder">';
                                    item += '<img class="il-photo__img vidplaceholder" src="' + videothumb + '" data-filter="' + thisMedia.filter + '" />';
                                    item += '<video poster="' + videothumb + '" preload="none" controls>';
                                    item += '<source src="' + src + '" type="video/mp4;"></source>';
                                    item += '</video>';
                                    item += '</div>';
                                }
                                if (item) {
                                    item = '<div class="col-sm-3 col-md-3">' + item + '</div>';
                                }
                                if (item !== '') {
                                    s.el.append(item);
                                    count++;
                                }
                                if (count == s.limit) {
                                    break;
                                }
                            }
                        }
                    },
                    error: function () {
                    }
                });
            }
        }
    }
    //Initialize twitter feed
    window.twttr = (function (d, s, id) {
        var t, js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);
        return window.twttr || (t = {_e: [], ready: function (f) {
                t._e.push(f)
            }});
    }(document, "script", "twitter-wjs"));

    $('.social-feeds-wrap').each(function (index, value) {

        var $target = $(this).find(".js-instafeed");
        instagram.loadContent({
            el: $target,
            clientID: $target.data('client-id'),
            limit: $target.data('count')
        });
    });

    var destroySlider = function (slider) {

        if (slider && slider.data('flexslider')) {
            var slider = slider.data('flexslider');
            var classNamespace = '.' + slider.vars.namespace; // Namespaced class selector
            if (slider.vars.controlNav)
                slider.controlNav.closest(classNamespace + 'control-nav').remove(); // Remove control elements if present
            if (slider.vars.directionNav)
                slider.directionNav.closest(classNamespace + 'direction-nav').remove(); // Remove direction-nav elements if present
            if (slider.vars.pausePlay)
                slider.pausePlay.closest(classNamespace + 'pauseplay').remove(); // Remove pauseplay elements if present
            slider.find('.clone').remove(); // Remove any flexslider clones
            slider.unbind(slider.vars.eventNamespace); // Remove events on slider
            if (slider.vars.animation != "fade")
                slider.container.unwrap(); // Remove the .flex-viewport div
            slider.container.removeAttr('style') // Remove generated CSS (could collide with 3rd parties)
            slider.container.unbind(slider.vars.eventNamespace); // Remove events on slider
            slider.slides.removeAttr('style'); // Remove generated CSS (could collide with 3rd parties)
            slider.slides.filter(classNamespace + 'active-slide').removeClass(slider.vars.namespace + 'active-slide'); // Remove slide active class
            slider.slides.unbind(slider.vars.eventNamespace); // Remove events on slides
            $(document).unbind(slider.vars.eventNamespace + "-" + slider.id); // Remove events from document for this instance only
            $(window).unbind(slider.vars.eventNamespace + "-" + slider.id); // Remove events from window for this instance only
            slider.stop(); // Stop the interval
            slider.removeData('flexslider'); // Remove data
        }
    }
})